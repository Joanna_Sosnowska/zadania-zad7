# -*- coding: utf-8 -*-
__author__ = 'Joanna667'
from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone

class Article(models.Model):
    tytul = models.CharField(max_length=50)
    tresc = models.CharField(max_length=500)
    pub_date = models.DateTimeField()
    pub_user = models.ForeignKey(User, related_name='pub_users')

    def __unicode__(self):
        return self.tytul