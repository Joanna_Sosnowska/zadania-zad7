__author__ = 'Joanna667'
from django.conf.urls import patterns, url
import views

urlpatterns = patterns('',
    url(r'^$', views.index, name="index"),
    url(r'^users/(?P<idd>\d+)$', views.user_posts, name="userArticles"),
    url(r'^login$', views.log_in, name="login"),
    url(r'^logout$', views.log_out, name="logout"),
    url(r'^create$', views.create, name="create"),
    url(r'^', views.notfound,name='notfound')
)