#!/usr/bin/python
# -*- coding: utf-8 -*-
__author__ = 'Joanna667'

from django.db import connection
from django.contrib.auth.models import User
from models import Article
from django.test import TestCase
from django.utils import timezone
from django.test import Client
import re

login = "Joanna667"
haslo = "floreczka1"
client = Client()


#test, czy dany template istnieje
class TemplatesTestCase(TestCase):
    def test_admin(self):
        response = client.get("/admin/")
        #print response
        self.assertEqual(response.status_code, 200)

    def test_home(self):
        response = client.get("/blog/")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Najnowsze')


    def test_login_form(self):
        response = client.get("/blog/users/login")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Zaloguj')

    def test_add_article_form(self):
        response = client.get("/blog/create")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'dla zalogowanych')


    def test_error(self):
        response = client.get("/blog/fiufiu")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'pusta')


#test dla konkretnych funcjonalności
class FuncionalityTestCase(TestCase):
    # baza danych - czy istnieje
    def test_database(self):
        cursor = connection.cursor()
        cursor.execute('SELECT * FROM microblog_article;')
        self.assertNotEqual(cursor.fetchall(), None)

    def create_user(self):
        return User.objects.create_user(
            username=login, password=haslo, email='kotka5351@gmail.com', first_name='Joanna',
            last_name='Sosnowska'
        )

    def test_messages(self):

        user = self.create_user()
        user.save()

        art = Article(
            tytul='tytul',
            tresc='tekst',
            pub_date=timezone.now(),
            pub_user=user

        )
        art.save()

        response = client.get("/blog/")

        self.assertContains(response, 'tytul')
        self.assertNotEqual(response.status_code, 404)
        self.assertNotEqual(response.content, None)

        response = client.get("/blog/users/1")
        self.assertContains(response, login)
        self.assertNotEqual(response.status_code, 404)

    def test_bad_login(self):
        response = self.client.post('/blog/login/', {'username': 'john', 'password': 'smith'})
        self.failUnlessEqual(response.status_code, 200)
        if not ('Zla nazwa uzytkownika lub haslo' in response):
            self.fail()

    def test_login(self):
        response = self.client.post('/blog/login/', {'username': 'Joanna', 'password': 'floreczka1'})
        self.failUnlessEqual(response.status_code, 302)
        if not ('zalogowany' in response):
            self.fail()
        response = self.client.post('/blog/create/', {'tytul': 'Drugi', 'tresc': 'jeszcze jeden wpis'})
        self.failUnlessEqual(response.status_code, 200)
        if not ('jeszcze jeden wpis' in response):
            self.fail()
        response = client.get("/blog/logout")
        if not ('Wylogowano' in response) :
            self.fail()
        response = client.get("/blog/users/1")
        if not ('jeszcze jeden wpis' in response):
            self.fail()













