# -*- coding: utf-8 -*-
__author__ = 'Joanna667'
from django.contrib import admin
from microblog.models import Article


class ArticleAdmin(admin.ModelAdmin):
    ordering = ('pub_date', )
    list_filter = ('pub_date', )

admin.site.register(Article, ArticleAdmin)
