#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = 'Joanna667'


from models import Article
from forms import LoginForm, ArticleForm
from django.shortcuts import render_to_response, redirect
from django.http import Http404
from django.core.urlresolvers import reverse
import random, string
from django.template import RequestContext
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.utils import timezone


def log_in(request):
    if request.user.is_authenticated():
        return index(request)

    if request.method == "GET":
        form = LoginForm()

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=request.POST.get('username'), password=request.POST.get('password'))
            if user:
                login(request, user)
                return index(request, 'Udało się, jesteś teraz zalogowany')
            else:
                message = 'Zla nazwa uzytkownika lub haslo!'
    return render_to_response('login.html', locals(), RequestContext(request))


def log_out(request):
    if request.user.is_authenticated():
        logout(request)
        return index(request, 'Wylogowano!')
    return index(request)


def index(request, message=''):
    users = User.objects.all()
    if len(users) == 0:
        user1 = User()
        user1.username = "Joanna"
        user1.set_password("floreczka1")
        user1.email = "kotka5351@gmail.com"
        user1.is_active = True
        user1.is_superuser = True
        user1.is_staff = True
        user1.save()
        user2 = User()
        user2.username = "Joanna667"
        user2.set_password("figazmakiem")
        user2.email = "kotka5351@gmail.com"
        user2.is_active = True
        user2.save()
        user3 = User()
        user3.username = "Guest"
        user3.set_password("qwerty123")
        user3.email = "kotka5351@gmail.com"
        user3.is_active = True
        user3.save()
    lista = Article.objects.order_by('-pub_date')
    return render_to_response('index.html', locals(), RequestContext(request))

def create(request):
    if not request.user.is_authenticated():
        return index(request, 'Tylko dla zalogowanych')

    if request.method == "GET":
        form = ArticleForm()

    if request.method == "POST":
        form = ArticleForm(request.POST)
        if form.is_valid():
            art = Article(tytul=form.cleaned_data['tytul'],
                        tresc=form.cleaned_data['tresc'],
                        pub_user=request.user,
                        pub_date=timezone.now(),
            )
            art.save()
            return redirect(reverse('index'), 'Dodano wpis!')

    return render_to_response('create.html', locals(), RequestContext(request))

def user_posts(request, idd):
    pub_user = User.objects.all().filter(id=idd)
    if pub_user:
        pub_user = pub_user[0]
        lista = Article.objects.all().filter(pub_user_id=idd).order_by('-pub_date')
        return render_to_response('user.html', locals(), RequestContext(request))
    raise Http404()

def notfound(request):
    info = 'Nie znaleziono strony'
    return render_to_response('notfound.html', locals(), RequestContext(request))
