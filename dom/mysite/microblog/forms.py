# -*- coding: utf-8 -*-

__author__ = 'Joanna667'
from django.utils import timezone
from django.contrib.auth.models import User
from models import Article
from django import forms

class LoginForm(forms.Form):
    username = forms.CharField(max_length=50, widget=forms.TextInput())
    password = forms.CharField(max_length=50, widget=forms.PasswordInput(render_value=False))

class ArticleForm(forms.Form):
    tytul = forms.CharField(max_length=40, min_length=3)
    tresc = forms.CharField(max_length=300, min_length=10, widget=forms.Textarea())
