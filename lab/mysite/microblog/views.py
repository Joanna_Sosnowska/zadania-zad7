# -*- encoding: utf-8 -*-
from django.shortcuts import render_to_response
from forma import ArticleForm
from models import Article as A
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf
from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from django.template import RequestContext


def index(request):
    lista = A.objects.all()
    return render(request, 'index.html', {'articles': lista})


def created(request):
    try:
        if request.method == 'POST':
            form = ArticleForm(request.POST)
            print form
            if form.is_valid():
                tytul = form.cleaned_data['tytul']
                tresc = form.cleaned_data['tresc']
                autor = form.cleaned_data['autor']
                b = A(autor=autor, tytul=tytul, tresc=tresc)
                b.save()

            else:
                return render(request, 'addArticle.html', {'wiad': 'Nalezy wypełnic oba pola'})
        else:
            return render(request, 'error.html', {'info': 'Error'})
        return render(request, 'added-success.html', {'tytul': tytul, 'tresc': tresc})
    except KeyError:
        return render(request, 'error.html', {'info': 'Unexpected error'})


def err(request):
    return render(request, 'error.html', {'info': 'Unexpected error'})


def article(request, idd = 1):
    try:
        return render_to_response('article.html',
            {'wpis': A.objects.get(id=idd)},
                    context_instance=RequestContext(request),)
    except:
        return render(request, 'error.html', {'info': 'Nie ma takiego wpisu'})

def notfound(request):
    return render(request, 'notfound.html', {'info': 'Nie znaleziono strony'})



