# -*- encoding: utf-8 -*-
from django.db import models


# Create your models here.
class Article(models.Model):
    tytul = models.CharField(max_length=100)
    tresc = models.CharField(max_length=500)
    autor = models.CharField(max_length=50)

    def __unicode__(self):
        return self.tytul
