__author__ = 'Joanna667'

import unittest
from django.db import connection
from models import Article
from django.test import TestCase
from django.utils import timezone
from django.test import Client
import re

client = Client()


#test, czy dany template istnieje
class TemplatesTestCase(TestCase):
    def test_home(self):
        response = client.get("/blog/")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Wpisz')
        self.assertContains(response, 'Nie ma')

    def test_add_article_form(self):
        response = client.get("/blog/addArticle")
        self.assertEqual(response.status_code, 200)

    def test_error(self):
        response = client.get("/blog/fiufiu")
        #print response
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'pusta')


#test konkretnych funkcjonalnosci
class FuncionalityTestCase(unittest.TestCase):
    def test_adding_article(self):
        Article(tytul="Tytul", tresc="Tresc", autor="Joanna").save()
        response = client.get("/blog/")
        self.assertContains(response, 'Wpisz')
        self.assertContains(response, 'Joanna')
        self.assertTrue(len(Article.objects.all()) == 1)
        response = client.get("/blog/article/1")
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Joanna')

    def test_adding_article2(self):
        response = self.client.post('/blog/addArticle/', {'tytul': 'Drugi', 'tresc': 'jeszcze jeden wpis', 'autor':'Joanna'})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'dodany')
        self.assertContains(response, 'Drugi')

    def test_adding_article_error(self):
        response = self.client.post('/blog/addArticle/', {'tytul': 'Drugi', 'tresc': '', 'autor':''})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Nalezy wypełnic oba pola')




if __name__ == '__main__':
    unittest.main()
