# -*- encoding: utf-8 -*-
from django.contrib import admin
admin.autodiscover()#wykrywa modele
from django.conf.urls import patterns, url
from django.views.generic import TemplateView
import views

urlpatterns = patterns('',
    url(r'^$', views.index,name='index'),
    url(r'^addArticle/$',TemplateView.as_view(template_name='addArticle.html'),name='addArticle'),
    url(r'^added-success/$',views.created,name='added-success'),
    url(r'^error/$',views.err,name='error'),
    url(r'^article/(?P<idd>\d+)$',views.article,name='article'),

    url(r'^', views.notfound,name='notfound'),

)
