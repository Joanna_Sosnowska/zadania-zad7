# -*- encoding: utf-8 -*-
__author__ = 'st'
from django import forms
from models import Article


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('tytul', 'tresc', 'autor')

